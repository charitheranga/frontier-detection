#include <freespace_frontier_extractor.h>
#include <freespace_frontier_representative.h>

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/features/normal_3d.h>

#include <iostream>

using namespace octomap_frontiers3d;

//constructor
FreeSpaceFrontierExtractor::FreeSpaceFrontierExtractor(const ros::NodeHandle &nh,
                                                       const ros::NodeHandle &nh_private,
                                                       double cluster_radius,
                                                       double neighborhood_radius)

    : nh_(nh),
      nh_private_(nh_private),
      cluster_radius_(cluster_radius),
      neighborhood_radius_(neighborhood_radius),
      free_voxels_(NULL)

{
    //UpdateClusterSizeThreshold();

    extractorService_ = nh_.advertiseService("freespace_frontier_detector",
                                             &octomap_frontiers3d::FreeSpaceFrontierExtractor::extractorCallback,
                                             this);
}

//destructor
FreeSpaceFrontierExtractor::~FreeSpaceFrontierExtractor()
{
    if (free_voxels_ != NULL)
        delete free_voxels_;
}

bool FreeSpaceFrontierExtractor::extractorCallback(frontier_detection::frontier_srv::Request &req,
                                                   frontier_detection::frontier_srv::Response &res)
{
    ros::Time computationTime = ros::Time::now();
    // Check that planner is ready to compute path.
    if (!ros::ok())
    {
        ROS_INFO_THROTTLE(1, "Exploration finished. Not planning any further moves.");
        return true;
    }

    /////////////////////////////////////////
    ///add the code here.
    /////////////////////////////////////////

    ROS_INFO("frontier extraction computation lasted %2.3fs", (ros::Time::now() - computationTime).toSec());
    return true;
}

//setter api
void FreeSpaceFrontierExtractor::SetOcTree(octomap::OcTree **octree)
{
    octree_ = *octree;
    resolution_ = octree_->getResolution();
    UpdateClusterSizeThreshold();
}

// api method
void FreeSpaceFrontierExtractor::Extract(std::vector<octomap_frontiers3d::FreeSpaceFrontierRepresentative> &fs_freps,
                                         PointXYZListPtr freespace_frontier_voxels)
{
    if (!octree_)
        return;

    DetectFrontierVoxels();
    std::vector<FVClusterIndicesPtr> clusters;
    ClusterFrontierVoxels(clusters, fs_freps);
    fs_freps_ = fs_freps;
    if (freespace_frontier_voxels)
        BOOST_FOREACH (pcl::PointXYZ &p, freespace_frontier_voxel_centers_cloud_->points)
            freespace_frontier_voxels->push_back(p);
}

// internal method
void FreeSpaceFrontierExtractor::DetectFrontierVoxels()
{
    // REMOVED
}

//internal method
void FreeSpaceFrontierExtractor::ClusterFrontierVoxels(std::vector<FVClusterIndicesPtr> &clusters, std::vector<octomap_frontiers3d::FreeSpaceFrontierRepresentative> &fs_freps)
{
    // REMOVED
}

bool FreeSpaceFrontierExtractor::VoxelClusterReachedRadiusLimit(const octomap::KeySet &cluster_frontier, const octomap::KeySet &cluster)
{
    Eigen::Vector3f mean, evs;
    Eigen::Matrix3f pcs;
    ComputeClusterPrincipalComponents(cluster_frontier, mean, evs, pcs);

    Eigen::Vector3f pc_0 = pcs.col(0); // retrieve the 1st principal component
    // project each voxel point to this principal component and find the maximum
    // distance from mean of cluster to the frontier along principal component
    for (octomap::KeySet::const_iterator cfi = cluster_frontier.begin();
         cfi != cluster_frontier.end(); cfi++)
    {
        pcl::PointXYZ p = Point3DOctomapToPCL<pcl::PointXYZ>(octree_->keyToCoord(*cfi));
        double x = fabs((p.getVector3fMap() - mean).dot(pc_0));
        if (x > cluster_radius_) // if the projected distance is larger than cluster radius, stop growing the cluster
            return true;
    }

    return false;
}

void FreeSpaceFrontierExtractor::ComputeClusterPrincipalComponents(const octomap::KeySet &cluster_frontier, Eigen::Vector3f &mean, Eigen::Vector3f &evs, Eigen::Matrix3f &pcs)
{
    // retrieve the point vectors and the mean vector
    std::vector<Eigen::Vector3f> cfps;
    mean = Eigen::Vector3f::Zero();
    for (octomap::KeySet::const_iterator cfi = cluster_frontier.begin();
         cfi != cluster_frontier.end(); cfi++)
    {
        pcl::PointXYZ p = Point3DOctomapToPCL<pcl::PointXYZ>(octree_->keyToCoord(*cfi));
        cfps.push_back(p.getVector3fMap());
        mean += p.getVector3fMap();
    }
    mean /= cfps.size();

    // compute the covariance matrix
    Eigen::Matrix3f A = Eigen::Matrix3f::Zero();
    BOOST_FOREACH (Eigen::Vector3f p, cfps)
        A += (p - mean) * ((p - mean).transpose());
    A /= cfps.size();

    // do eigen analysis of covariance matrix
    Eigen::SelfAdjointEigenSolver<Eigen::Matrix3f> es;
    es.compute(A);

    // sort eigen vectors from first principal component
    Eigen::Matrix3f V;
    for (int i = 0; i < 3; i++)
        V.col(i) = es.eigenvectors().col(2 - i);

    pcs = V;
    evs = es.eigenvalues();
}

//api method
octomap_frontiers3d::FreeSpaceFrontierRepresentative
FreeSpaceFrontierExtractor::GenerateFreeSpaceFrontierRepresentative(FVClusterIndicesPtr &cluster_indices, pcl::search::KdTree<pcl::PointXYZ>::Ptr &nn_tree)
{
 // REMOVED 
}

//internal method
void FreeSpaceFrontierExtractor::UpdateClusterSizeThreshold()
{
    double voxel_face_area = pow(resolution_, 2);
    double expected_cluster_area = M_PI * cluster_radius_ * cluster_radius_;
    min_no_of_voxels_in_cluster_ = static_cast<int>(floor((0.5 * expected_cluster_area) / voxel_face_area));
}

//internal method
void FreeSpaceFrontierExtractor::OrientFreeSpaceFrontierNormalTowardsEmptySpace(octomap_frontiers3d::FreeSpaceFrontierRepresentative &fs_frep)
{
    Eigen::Vector3f sp(fs_frep.p.x, fs_frep.p.y, fs_frep.p.z);    // source point
    Eigen::Vector3f n_pos(fs_frep.n.x, fs_frep.n.y, fs_frep.n.z); // positive normal
    Eigen::Vector3f n_neg = -n_pos;                               // negative normal
    Eigen::Vector3f n = n_pos;                                    // resultant normal, initialized to positive normal

    double step_size = octree_->getResolution();
    int limit = static_cast<int>(ceil(cluster_radius_ / step_size)); // number of times the stepping should be conducted
    int i = 0;
    while (i < limit)
    {
        i++;
        // step in positive and negative direction of the normal vector
        Eigen::Vector3f p1 = sp + i * step_size * n_pos;
        Eigen::Vector3f p2 = sp + i * step_size * n_neg;

        // find voxels containing p1 and p2
        octomap::OcTreeNode *n1 = octree_->search(p1.x(), p1.y(), p1.z());
        octomap::OcTreeNode *n2 = octree_->search(p2.x(), p2.y(), p2.z());

        if (n1 == NULL && n2 == NULL)
            continue;
        else if (n1 == NULL) // if p1 is in unknown
        {
            n = n_pos;
            break;
        }
        else if (n2 == NULL) // if p2 is in unknown
        {
            n = n_neg;
            break;
        }
    }

    // update the normal vector
    fs_frep.n.x = n.x();
    fs_frep.n.y = n.y();
    fs_frep.n.z = n.z();
}

//setter
void FreeSpaceFrontierExtractor::SetClusterRadius(double cluster_radius)
{
    cluster_radius_ = cluster_radius;
    UpdateClusterSizeThreshold();
}

//getter
double
FreeSpaceFrontierExtractor::GetClusterRadius()
{
    return cluster_radius_;
}

//setter
void FreeSpaceFrontierExtractor::SetVoxelNeighborhoodRadius(double neighborhood_radius)
{
    neighborhood_radius_ = neighborhood_radius;
}

//gettter
double
FreeSpaceFrontierExtractor::GetVoxelNeighborhoodRadius()
{
    return neighborhood_radius_;
}

//internal method
uint32_t FreeSpaceFrontierExtractor::ExtractFreeVoxelKeys(octomap::OcTree *octree,
                                                          octomap::KeySet &free_voxels)
{
    free_voxels.clear();
    uint32_t vox_count = 0;

    uint32_t tree_depth = octree->getTreeDepth();

    for (octomap::OcTree::iterator it = octree->begin(tree_depth),
                                   end = octree->end();
         it != end; it++)
    {
        if (octree->isNodeOccupied(*it))
            continue;

        octomap::KeySet key_set;
        uint32_t depth = it.getDepth();
        octomap::OcTreeKey key = it.getKey();

        if (depth < tree_depth)
            ExtractAllBasicVoxels(key, depth, tree_depth, key_set);
        else
            key_set.insert(key);

        free_voxels.insert(key_set.begin(), key_set.end());

        vox_count += key_set.size();
    }

    return vox_count;
}

//internal method
void FreeSpaceFrontierExtractor::ExtractAllBasicVoxels(octomap::OcTreeKey key,
                                                       uint32_t depth,
                                                       uint32_t max_tree_depth,
                                                       octomap::KeySet &key_set)
{
    // get the leaf voxel's (cube's) size in smallest voxels (i.e. resolution)
    int cube_size_in_voxels = (max_tree_depth - depth) << 1;

    int s = cube_size_in_voxels / 2;

    // for voxels with depth < max_tree_depth, their key corresponds is
    // generated from bottom south west coordinate of the top north east octant

    for (int i = -s; i < s; i++)
    {
        for (int j = -s; j < s; j++)
        {
            for (int l = -s; l < s; l++)
            {
                octomap::OcTreeKey nkey;
                nkey.k[0] = key.k[0] + i;
                nkey.k[1] = key.k[1] + j;
                nkey.k[2] = key.k[2] + l;
                key_set.insert(nkey);
            }
        }
    }
}